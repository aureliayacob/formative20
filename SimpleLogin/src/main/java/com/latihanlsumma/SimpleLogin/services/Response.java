package com.latihanlsumma.SimpleLogin.services;

import java.util.List;

import com.latihanlsumma.SimpleLogin.model.User;
public class Response {
	private int status;
	private String message;
	private List<User> data;
	
	public Response(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	public Response(int status, String message, List<User> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<User> getData() {
		return data;
	}
	public void setData(List<User> data) {
		this.data = data;
	}


}
