package com.latihanlsumma.SimpleLogin.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.latihanlsumma.SimpleLogin.model.User;

public interface LoginRepository extends JpaRepository<User,Long>{

	@Query(value="select * from user a where a.user_id= ?1", nativeQuery=true)
	List<User>  getUserByID(Long id);

	@Query(value="select * from User a", nativeQuery=true)
	List<User> getAllUser();

	@Transactional
	@Modifying
	@Query(value="insert into user (name, password) values (?1,?2)", nativeQuery=true)
	int createUser(String name, String password);

	@Query(value="select a.name from user a", nativeQuery=true)
	List<String> getAllUserNames();

	@Query(value="select a.password from user a where a.name= ?1", nativeQuery=true)
	String getPasswordByUSN(String usn); //get passwd by username
	
	@Query(value="select * from user a where a.name= ?1", nativeQuery=true)
	List<User>  getUserByUsername(String usn);



}
