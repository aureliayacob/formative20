package com.latihanlsumma.SimpleLogin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.latihanlsumma.SimpleLogin.model.User;
import com.latihanlsumma.SimpleLogin.services.LoginRepository;
import com.latihanlsumma.SimpleLogin.services.Response;

import at.favre.lib.crypto.bcrypt.BCrypt;

@RestController
public class LoginController {
	List<User> data = new ArrayList<User>();

	@Autowired
	LoginRepository repo;

	@GetMapping(value="/api/users")
	public ResponseEntity<Response> getAllData(){
		data = repo.getAllUser();
		if(data.size()<0) { //Handle Error
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(500,"Data Not Found",formatData(data)));
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(201, "OK", data));
		}
	}

	@GetMapping(value="/api/user/{id}")
	public ResponseEntity<Response> getUserByID(@PathVariable int id){
		data = repo.getUserByID(Long.valueOf(id));

		if(data.size()==0) { //Handle Error

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(500,"User Not Found",data));
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(201, "User Found", data));
		}
	}

	@PostMapping(value="/api/user", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> createUser(@RequestBody User payload){
		String name = payload.getName();
		if(verifyUsername(name)==false) {
			String passwordHash = generatePasswordHash(payload.getPassword());
			repo.createUser(name,passwordHash);
			data=repo.getUserByUsername(name);
			return ResponseEntity.status(HttpStatus.OK).body(new Response(200,"Create Account SUCCESS",formatData(data)));
		}
		List<User> data = new ArrayList<User>();
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Response(406,"Create Account FAILED, Username Already Exist...",data));
	}

	public boolean verifyUsername(String toCheck) {
		List<String> allUsn = repo.getAllUserNames();
		for(String usn : allUsn) {
			if(toCheck.equalsIgnoreCase(usn)) { 
				return true; //ada yg sama
			}
		}
		return false;//gak ada
	}

	@PostMapping(value="/api/auth/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> login(@RequestBody User payload){
		if(verifyUsername(payload.getName())==true) {
			if(verifyPassword(payload)) {
				return ResponseEntity.status(HttpStatus.ACCEPTED).body(new Response(202,"Congratulations LOGIN SUCCESS!", formatData(data)));	
			}else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new Response(401,"Login FAILED, Wrong Password..."));
			}
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new Response(401,"Login FAILED, Username doesn't match any account"));
		}
	}


	public String generatePasswordHash(String newPassword) {
		String bcryptHashString = BCrypt.withDefaults().hashToString(12, newPassword.toCharArray());
		return bcryptHashString;
	}
	public boolean verifyPassword(User payload) {
		String passwordFromDB = repo.getPasswordByUSN(payload.getName());
		BCrypt.Result result = BCrypt.verifyer().verify((payload.getPassword()).toCharArray(), passwordFromDB);
		return result.verified;
	}

	public List<User> formatData(List<User> users){
		List<User> formatted = new ArrayList<User>();
		for(User u : users) {
			formatted.add(new User(u.getUser_id(),u.getName(), ""));
		}
		return formatted;
	}
}
