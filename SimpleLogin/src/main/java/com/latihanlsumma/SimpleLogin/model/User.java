package com.latihanlsumma.SimpleLogin.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private int user_id;
	private String name;
	private String password;

	protected User() {
		super();
	}
	
	public User( int id, String name, String password) {
		super();
		this.user_id = id;
		this.name = name;
		this.password = password;
	}
	
	public User( String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "User [id=" + user_id + ", name=" + name + "]";
	}
	
	

}
